package api.postcodes;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class PostCodeStepDefinitions {

    @Steps
    PostCodeAPI postCodeAPI;

    @When("I look up a post code {word} for country code {word}")
    public void lookUpAPostCode(String postCode, String countryCode) {
        postCodeAPI.getLocationByPostCodeAndCountry(postCode, countryCode);
    }

    @Then("the location should be {} in {}")
    public void theLocationShouldBe(String placeName, String state) {
        restAssuredThat(validatableResponse -> validatableResponse.statusCode(200));
        restAssuredThat(validatableResponse -> validatableResponse.body(LocationResponse.STATE, equalTo(state)));
        restAssuredThat(validatableResponse -> validatableResponse.body(LocationResponse.PLACE_NAME, equalTo(placeName)));

    }

    @Then("response status code should be <{int}>")
    public void responseStatusCodeShouldBe(int statusCode) {
        restAssuredThat(validatableResponse -> validatableResponse.statusCode(statusCode));
    }
}
