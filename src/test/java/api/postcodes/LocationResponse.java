package api.postcodes;

public class LocationResponse {
    public static final String STATE = "'places'[0].'state'";
    public static final String PLACE_NAME = "'places'[0].'place name'";
}
