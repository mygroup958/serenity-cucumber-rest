Feature: Looking up post codes

  Scenario Outline: Check locations by post code
    When I look up a post code <Post Code> for country code <Country Code>
    Then the location should be <Place Name> in <State>
    Examples:
      | Post Code | Country Code | State       | Place Name    |
      | 10000     | US           | New York    | New York City |
      | 90210     | US           | California  | Beverly Hills |


  Scenario: Check response by invalid post code
    When I look up a post code <902101> for country code <US>
    Then response status code should be <404>

