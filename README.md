## This is a project for Serenity with Cucumber and RestAssured

### Preconditions:
Java version - 11, git and Maven version - 3.8.2 should be installed

To install the project, download it on Gitlab:
```
git clone https://gitlab.com/mygroup958/serenity-cucumber-rest.git
```

## For tests run
-  from commandline: mvn clean verify
-  also you can run the CucumberTestSuite test runner class
-  The test results will be recorded in the target/site/serenity directory.

## To write new tests:
1. Add new scenario in "looking_up_post_codes.feature" using the given..when..then structure ('Gherkin' format)
```
Scenario: Check response by invalid post code
When I look up a post code <902101> for country code <US>
Then response status code should be <404>
   ```
   
   
2. In Cucumber, each line of the Gherkin scenario maps to a method in a Java class - 'PostCodeStepDefinitions'.
   These use annotations like @Given, @When and @Then match lines in the scenario to Java methods.
   
```
@Then("response status code should be <{int}>")
public void responseStatusCodeShouldBe(int statusCode) {
restAssuredThat(validatableResponse -> validatableResponse.statusCode(statusCode));
}
```





   

